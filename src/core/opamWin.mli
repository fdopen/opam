include module type of Extwin_opam

(** currently dash.exe with "-ec" is used, in order to execute
    configure scripts and similar cygwin based scripts.
    dash is slightly faster than bash and is already the default
    shell on debian based distros.
*)
val shell_cmd : string
val shell_cmd_prepend_args : string list

(** specialised version of Ext_unix.cygwin_shell_rewrite *)
val cygwin_shell_rewrite : force_shell:bool -> string -> string list -> string * string list

(** adds an exe extension on Windows only *)
val add_exe : string -> string

(** returns the unmodified string under *nix,
    translates the string to a cygwin path under
    windows *)
val cygpath : string -> string

(**
   There are several kinds of environment variables:

  - environment variables, that are ignored by cygwin programs
  - environment variables, that are treated the same way by windows and cygwin
  - environment variables, that are exclusively used by cygwin programs, e.g MANPATH
  - magic environment variables, e.g. PATH, TEMP, TMP

    PATH has usually the form of
    "C:\\cygwin\\bin;C:\\Windows\\system32;...", however cygwin based
    programs are under the illusion PATH would be
    "/bin:/cygdrive/c/system32:...".  They are 'magically' transformed, if
    a native windows program calls a cygwin program and vice versa.

    When a cygwin based program (like a shell in case of eval `opam
    config env`) parses our input, we must present the content of
    these variables in a form that is natural to them.

    The following functions should deal with this issue.
    It's currently work in progress :)
*)
val is_cygwin_env : string -> bool
val shell_env_var : key:string -> string -> string
