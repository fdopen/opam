include Extwin_opam
open Extwin_dirty
let shell_cmd = "dash.exe"
let shell_cmd_prepend_args = ["-ec"]

let is_win = Sys.os_type = "Win32"
let () =
  if is_win then (
    (try set_binary_mode_in stdin true with | Sys_error _ -> ());
    (try set_binary_mode_out stdout true with | Sys_error _ -> ());
    (try set_binary_mode_out stderr true with | Sys_error _ -> ());
  )

let cygwin_shell_rewrite ~force_shell prog args =
  cygwin_shell_rewrite
    ~shell_cmd
    ~shell_args:shell_cmd_prepend_args
    ~force_shell
    prog
    args

let cygpath x = cygpath (fun _ -> ()) x

let add_exe s =
  if is_win = false || Filename.check_suffix s ".exe" then
    s
  else
    s ^ ".exe"

let is_cygwin_env x =
  match x with
  | "TMP" | "TEMP" | "PATH" | "MANPATH" -> true
  | _ -> false

let path_sep_rex = Re_pcre.regexp ";"
let shell_env_var ~key value =
  if is_win = false then
    value
  else
    match key with
    (* TODO: what else must be handled beyond PATH ? *)
    | "TMP" -> cygpath value
    | "TEMP" -> cygpath value
    | "PATH" | "MANPATH" ->
      let sep = ":" in
       let l = Re_pcre.split ~rex:path_sep_rex value |> List.filter ((<>) "") in
       (* respect cygwin conventions: /usr/bin is identical to /bin , but twice
          in path, /usr/bin first, than /bin.
          TODO: use ORIGINAL_PATH and mount table?*)
      let l =
        if key <> "PATH" then
          List.map (fun x -> slashify x |> cygpath ) l
        else
        let f (usr_bin_added,accu) el =
          match slashify el |> cygpath with
          | "/usr/bin" ->
            if usr_bin_added then
              true,("/bin"::accu)
            else
              true,("/usr/bin"::accu)
          | x -> usr_bin_added,(x::accu)
        in
        List.fold_left f (false,[]) l |> snd |> List.rev
      in
      String.concat sep l
    | _ -> value
